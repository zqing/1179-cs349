import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.*;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;

//////////////// Model ////////////////

class DrawingModel extends Observable
{
	static double[] closestPoint(double P0[], double P1[], Point2D.Double M) {
		double v[] = new double[2];
		v[0] = P1[0] - P0[0];
		v[1] = P1[1] - P0[1];
		// early out if line is less than 1 pixel long
		if ( v[0] * v[0] + v[1] * v[1]  < 1 ) return P0;
		double u[] = new double[2];
		u[0] = M.getX() - P0[0]; // u = M - P1
		u[1] = M.getY() - P0[1];
		// scalar of vector projection ...
		double s = ( v[0] * u[0] + v[1] * u[1] ) / ( v[0] * v[0] + v[1] * v[1] );
		// find point for constrained line segment
		if (s < 0) return P0;
		if (s > 1) return P1;
		double proj[] = new double[2];
		proj[0] = P0[0] + s * v[0];
		proj[1] = P0[1] + s * v[1];
		return proj;
	}

	class Shape extends Path2D.Double {

		Color color = Color.BLACK;
		Stroke stroke = new BasicStroke(2);
		int numPoints = 0;
		int x=0,y=0;
		int scale = 10;
		int rotate = 0;
		boolean selected = false;
		// Cached data
		AffineTransform cache_at = null;
		java.awt.Shape cache_draw = null;

		public void paint(Graphics2D g)
		{
			generateDrawingCache();
			if(selected){
				g.setColor(Color.YELLOW);
				g.setStroke(new BasicStroke(7));
				g.draw(cache_draw);
			}
			g.setStroke(stroke);
			g.setColor(color);
			g.draw(cache_draw);
			//g.draw(getBounds2D());
		}

		public void generateDrawingCache(){
			if(cache_at == null) {
				cache_at = new AffineTransform();
				cache_at.translate(x, y);
				cache_at.scale(((double) scale) / 10, ((double) scale) / 10);
				cache_at.rotate(Math.toRadians(rotate));
				cache_draw = null;
			}
			if(cache_draw == null)
			{
				cache_draw = this.createTransformedShape(cache_at);
			}
		}

		public boolean hit(double x, double y)
		{
			generateDrawingCache();
			PathIterator it = cache_draw.getPathIterator(null);
			double coords[] = new double[6];
			double last[] = new double[2];
			Point2D.Double mouse = new Point2D.Double(x,y);
			while(!it.isDone()) {
				if(it.currentSegment(coords)==PathIterator.SEG_LINETO){
						double proj [] = closestPoint(last,coords,mouse);
						if(mouse.distance(proj[0],proj[1]) <= 8 ) return true;
				}
				last[0] = coords[0];
				last[1] = coords[1];
				it.next();
			}
			return false;
		}

		public void move(double dx, double dy)
		{
			x += dx; y += dy;
			if(x < 0) x = 0;
			if(y < 0) y = 0;
			cache_at = null;
		}
	}

	HashSet<Shape> shapes = new HashSet<>();
	Shape last_selected = null;
	Shape current_draw = null;
	Dimension preferred_size = null;

	DrawingModel(){setChanged();}

	public void addPoint(double x, double y)
	{
		if(current_draw != null){
			current_draw.lineTo(x, y);
			current_draw.cache_draw = null;
			preferred_size = null;
		}else{
			current_draw = new Shape();
			current_draw.moveTo(x, y);
		}
		current_draw.numPoints ++;
		setChanged();
		notifyObservers();
	}

	public void addShape()
	{
		if(current_draw != null){
			Rectangle2D box = current_draw.getBounds2D();
			AffineTransform at = new AffineTransform();
			at.translate(-box.getCenterX(),-box.getCenterY());
			current_draw.transform(at);
			current_draw.move( box.getCenterX(),box.getCenterY());
			shapes.add(current_draw);
			current_draw = null;
			preferred_size = null;
			setChanged();
		}
		notifyObservers();
	}

	public void deleteLastSelected()
	{
		if(last_selected != null)
		{
			shapes.remove(last_selected);
			last_selected = null;
			preferred_size = null;
			setChanged();
		}
		notifyObservers();
	}
	public Dimension getPreferredSize()
	{
		if(preferred_size == null) {
			preferred_size = new Dimension(0, 0);
			if(current_draw != null) shapes.add(current_draw);
			for (Shape s : shapes) {
				s.generateDrawingCache();
				Rectangle r = s.cache_draw.getBounds();
				Point2D.Double p = new Point2D.Double(r.getMaxX(),r.getMaxY());
				preferred_size.width = Integer.max(preferred_size.width, (int) p.x + 16);
				preferred_size.height = Integer.max(preferred_size.height, (int) p.y + 16);
			}
			if(current_draw != null) shapes.remove(current_draw);
		}
		return preferred_size;
	}

	public void moveLastSelected(double dx, double dy)
	{
		if(last_selected == null || (dx == 0 && dy == 0)) return;
		last_selected.move(dx,dy);
		preferred_size = null;
		setChanged();
		notifyObservers();
	}

	public void scaleLastSelected(int scale)
	{
		if(last_selected != null) {
			last_selected.scale = scale;
			last_selected.cache_at =null;
			preferred_size =null;
			setChanged();
		}
		notifyObservers();
	}
	public void rotateLastSelected(int rotate)
	{
		if(last_selected != null){
			last_selected.rotate = rotate;
			last_selected.cache_at =null;
			preferred_size = null;
			setChanged();
		}
		notifyObservers();
	}

	public void select(double x, double y)
	{
		for(Shape c : shapes){
			//if(c.intersects(x-8,y-8,16,16)) return c;
			if(c.hit(x,y)){
				c.selected = true;
				last_selected = c;
				setChanged();
				break;
			}
		}
		notifyObservers();
	}

	public void unselect()
	{
		for(Shape s:shapes) {
			if(s.selected) setChanged();
			s.selected = false;
		}
		last_selected = null;
		notifyObservers();
	}

}

//////////////// View ////////////////
class DrawingPanel extends JPanel
	implements Observer
{
	class MyPanel extends JPanel
	{
		@Override
		public void setEnabled(boolean b){ for(Component i: getComponents()) i.setEnabled(b); }
	}
	class MySlider extends MyPanel
	{
		JLabel text;
		JLabel value;
		JSlider slider;

		public MySlider(String text, int min, int max, int value, IntConsumer l, IntFunction<String> fmt) {
			this.slider = new JSlider(JSlider.HORIZONTAL, min, max, value);
			this.text = new JLabel(text);
			this.value = new JLabel(fmt.apply(slider.getValue()));
			add(this.text);
			add(this.slider);
			add(this.value);
			this.text.setMinimumSize(new Dimension(0,0));
			this.value.setMinimumSize(new Dimension(0,0));
			this.value.setText(fmt.apply(min));
			this.value.setPreferredSize(this.value.getMaximumSize() );
			this.value.setText(fmt.apply(value));
			this.slider.addChangeListener(
					e -> this.value.setText(fmt.apply(slider.getValue()))
			);
			this.slider.addChangeListener( e-> l.accept(slider.getValue()) );
		}

		public void setValue(int n) { slider.setValue(n); }

	}

	class Toolbar extends MyPanel
		implements Observer
	{
		JButton b_delete = new JButton("Delete");
		MySlider s_scale = new MySlider("Scale",5,20,10,
				i -> model.scaleLastSelected(i),
				i -> Double.toString((double)i/10)
				);
		MySlider s_rotate = new MySlider("Rotate",-180,180,0,
				i -> model.rotateLastSelected(i),
				Integer::toString
				);

		Toolbar() {
			this.add(b_delete);
			this.add(s_scale);
			this.add(s_rotate);
			//b_delete.setPreferredSize(new Dimension(20,5));
			this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
			b_delete.addActionListener((ActionEvent e) -> { model.deleteLastSelected();} );
		}

		@Override
		public void update(Observable o, Object arg) {
			if(model.last_selected == null)  super.setEnabled(false);
			else {
				s_scale.setValue(model.last_selected.scale);
				s_rotate.setValue(model.last_selected.rotate);
				super.setEnabled(true);
			}
		}
	}

	class Canvas extends MyPanel
	{
		Canvas()
		{
			MouseAdapter l = new MouseAdapter()
			{
				double x,y;
				@Override
				public void mousePressed(MouseEvent e) { x = e.getX(); y = e.getY();}
				@Override
				public void mouseDragged(MouseEvent e) {
					if(model.last_selected == null)  model.addPoint(e.getX(),e.getY());
					else model.moveLastSelected( e.getX() - x, e.getY() - y);
					x = e.getX(); y = e.getY();
				}
				@Override
				public void mouseReleased(MouseEvent e) { model.addShape(); }
				@Override
				public void mouseClicked(MouseEvent e){
					if(model.last_selected == null) model.select(e.getX(),e.getY());
					else model.unselect();
				}
			};
			this.setBackground(Color.WHITE);
			this.addMouseListener(l);
			this.addMouseMotionListener(l);
		}

		@Override
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			if(g2 == null) return;
			for(DrawingModel.Shape s: model.shapes) {
				s.paint(g2);
			}
			if( model.current_draw!=null ) model.current_draw.paint(g2);
		}

	}

	Toolbar toolbar = new Toolbar();
	Canvas canvas = new Canvas();
	JLabel statusbar = new JLabel();
	DrawingModel model = new DrawingModel();
	MessageFormat statusmsg = new MessageFormat(
			"{0,choice,0#No Stroke|1#1 Stroke|1<{0,number,integer} Strokes} {1}"
	);

	DrawingPanel(){
		super();
		setLayout(new BorderLayout(5,0));
		add(statusbar, BorderLayout.SOUTH);
		add(new JScrollPane(canvas), BorderLayout.CENTER);
		add(toolbar, BorderLayout.NORTH);
		statusbar.setBackground(Color.GRAY);
		model.addObserver(this);
		model.addObserver(toolbar);
		model.notifyObservers();
	}

	@Override
	public void update(Observable obj, java.lang.Object arg) {
		canvas.setPreferredSize(model.getPreferredSize());
		canvas.revalidate();
		Object [] msgargs= {model.shapes.size(),""};
		if(model.last_selected != null){
			msgargs[1] = String.format(
					", Selection( %d points, scale: %.1f, rotation: %d)",
					model.last_selected.numPoints,
					(double)model.last_selected.scale/10,
					model.last_selected.rotate
					);
		}
		statusbar.setText(statusmsg.format(msgargs));
		repaint();
	}

}

public class A2Basic
{
	//// Main
	public static void main(String[] args) {
		JFrame mainFrame = new JFrame("A2Basic");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setContentPane(new DrawingPanel());
		mainFrame.setSize(new Dimension(800,600));
		mainFrame.setVisible(true);
	}
}
