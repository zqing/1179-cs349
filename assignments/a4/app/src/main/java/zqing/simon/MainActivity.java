package zqing.simon;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity
    implements Observer
{


    private ViewGroup mSimonView;
    private View mWelcomeView;
    private TextView mMessageView;
    private Button mEnterButton;
    private Simon mSimon;
    private ColorStateList mDefaultButtonColor;
    private int mLastColor;
    private LayoutInflater mButtonFactory;
    private void updateButtons(){
        SeekBar numberSelector = findViewById(R.id.number_selector);
        int nbuttons = numberSelector.getProgress()+1;
        if(nbuttons != mSimon.getButtons()){
            mSimon.setButtons(nbuttons);
            mSimonView.removeAllViews();

            for(int i = 0; i < nbuttons;++i){
                final int id = i;
                mButtonFactory.inflate(R.layout.simon_button,mSimonView);
                View button = mSimonView.getChildAt(i);
                mDefaultButtonColor = button.getBackgroundTintList();
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("Click","Click id "+ id);
                        mSimon.verifyButton(id);
                    }
                });
                button.setEnabled(false);
            }
            numberSelector.setProgress(nbuttons - 1);
        }
    }

    private void updateLevel(){
        RadioGroup levelSelector = findViewById(R.id.level_selector);
        int interval;
        int checked = levelSelector.getCheckedRadioButtonId();
        switch (checked){
            case R.id.easy_button:
                interval = 1500;
                break;
            case R.id.hard_button:
                interval = 500;
                break;
            case R.id.normal_button:
            default:
                interval = 1000;
        }
        if(interval != mSimon.getInterval()) {
            mSimon.setInterval(interval);
            levelSelector.check(checked);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSimon = new Simon();
        mSimon.addObserver(this);

        setContentView(R.layout.activity_main);
        mWelcomeView = findViewById(R.id.welcome_frame);
        mSimonView = findViewById(R.id.simon_frame);
        mMessageView = findViewById(R.id.message_view);
        mEnterButton = findViewById(R.id.enter_button);
        mButtonFactory = getLayoutInflater();
        updateLevel();
        updateButtons();



        ((SeekBar)findViewById(R.id.number_selector)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) { updateButtons();}
            public void onStartTrackingTouch(SeekBar seekBar) {} public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        ((RadioGroup)findViewById(R.id.level_selector)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) { updateLevel(); }
        });

        findViewById(R.id.enter_button).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mSimon.newRound();
                return false;
            }
        });
    }

    private void setSimonButtonsEnable(boolean b){
        for(int i = 0; i < mSimonView.getChildCount(); ++i)
            mSimonView.getChildAt(i).setEnabled(b);
    }

    private void highlightSimonButtons(boolean set){
        for(int i = 0; i < mSimonView.getChildCount(); ++i) {
            mSimonView.getChildAt(i).setBackgroundTintList(mDefaultButtonColor);
        }
        if(set) {
            if (mLastColor == Color.rgb(255, 255, 0)) {
                mLastColor = Color.rgb(255, 191, 0);
            } else {
                mLastColor = Color.rgb(255, 255, 0);
            }
            mSimonView.getChildAt(mSimon.getDisplayButton())
                    .setBackgroundTintList(new ColorStateList(new int[][]{new int[]{0}}, new int[]{mLastColor}));
        }
    }

    @Override
    public void update(Observable obj, Object args)
    {
        Log.d("update","MainActivity update");
        Simon.State state = mSimon.getState();
        switch (state){
            case START:
                mWelcomeView.setVisibility(View.VISIBLE);
                mMessageView.setText(R.string.welcome_text);
                mEnterButton.setText(R.string.enter_button_start);
                setSimonButtonsEnable(false);
                break;
            case WIN:
                mWelcomeView.setVisibility(View.VISIBLE);
                mMessageView.setText(R.string.win_text);
                mMessageView.append("Score: "+ mSimon.getScore());
                mEnterButton.setText(R.string.enter_button_win);
                setSimonButtonsEnable(false);
                break;
            case LOSE:
                mWelcomeView.setVisibility(View.VISIBLE);
                mMessageView.setText(R.string.lose_text);
                mMessageView.append("Score: "+ mSimon.getScore());
                mEnterButton.setText(R.string.enter_button_lose);
                setSimonButtonsEnable(false);
                break;
            case COMPUTER:
                mWelcomeView.setVisibility(View.GONE);
                setSimonButtonsEnable(true);
                highlightSimonButtons(true);
                break;
            case HUMAN:
                mWelcomeView.setVisibility(View.GONE);
                setSimonButtonsEnable(true);
                highlightSimonButtons(false);
                break;
        }

    }


/*
    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mWelcomeView.setVisibility(View.GONE);
        mSimonView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mSimonView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                 | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
        mWelcomeView.setVisibility(View.VISIBLE);
    }
*/

}
