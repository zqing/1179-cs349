package zqing.simon;
import android.util.Log;

import java.util.Observable;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Simon extends Observable{
    enum State { START, COMPUTER, HUMAN, LOSE, WIN };
    private State state = State.START;
    private int score = 0;

    private int buttons;
    private int interval;

    private int length = 1;
    private int [] sequence ;
    private int index ;

    private Timer timer;

    public int getScore() { return score; }

    public int getButtons(){ return buttons; }
    public State getState(){ return state; }

    public int getInterval(){ return interval; }

    public int getDisplayButton() {
        if(state != State.COMPUTER) return -1;
        return sequence[index];
    }

    public void setButtons(int buttons )
    {
        if(this.buttons == buttons || this.state == State.COMPUTER || this.state == State.HUMAN)
            return;

        this.buttons = buttons;
        Log.d("Simon", "[DEBUG] reconfigure buttons to " + buttons + " displayable game" );

    }
    public void setInterval( int interval  )
    {
        if(this.interval == interval ||this.state == State.COMPUTER|| this.state == State.HUMAN)
            return;

        this.interval = interval;
        Log.d("Simon", "[DEBUG] reconfigure interval to " + interval + " displayable game" );
    }

    String getStateAsString() {
        switch (this.state) {
            case START:
                return "START";

            case COMPUTER:
                return "COMPUTER";

            case HUMAN:
                return "HUMAN";

            case LOSE:
                return "LOSE";

            case WIN:
                return "WIN";
            default:
                return "Unkown State";
        }
    }



    public void newRound() {
        if(state == State.COMPUTER || state == State.HUMAN) return;

        Random random = new Random();
        Log.d("Simon", "[DEBUG] newRound, Simon::state "
                + getStateAsString() );
        // reset if they lost last time
        if (state == State.LOSE) {
            Log.d("Simon", "[DEBUG] reset length and score after loss" );
            length = 1;
            score = 0;
        }


        Log.d("Simon", "[DEBUG] new sequence: ");
        sequence = new int[length];
        for (int i = 0; i < length; i++) {
            int b = random.nextInt(buttons);
            sequence[i]=b;
        }

        index = 0;
        state = State.COMPUTER;
        setChanged();
        notifyObservers(state);
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d("Simon","Computer: " + getDisplayButton());
                nextButton();
                if(state != State.COMPUTER){
                    timer.cancel();
                }

            }
        }, interval,interval);

    }

    private void nextButton() {
        if (state != State.COMPUTER) {
            Log.d("Simon","[WARNING] nextButton called in " + getStateAsString() );
            return ;
        }

         Log.d("Simon", "[DEBUG] nextButton:  index " + index +" displayable " + sequence[index]);

        // advance to next displayable
        index++;

        // if all the buttons were shown, give
        // the human a chance to guess the sequence
        if (index >= length) {
            index = 0;
            state = State.HUMAN;
        }
        setChanged();
        notifyObservers(state);

    }

    public boolean verifyButton(int button) {

        if (state != State.HUMAN) {
            Log.d("Simon", "[WARNING] verifyButton called in " + getStateAsString() );
            return false;
        }

        // did they press the right displayable?
        boolean correct = (button == sequence[index]);

        Log.d("Simon", "[DEBUG] verifyButton: index " + index
                    + ", pushed " + button
                    + ", sequence " + sequence[index]);


        // advance to next displayable
        index++;

        // pushed the wrong buttons
        if (!correct) {
            state = State.LOSE;

            Log.d("Simon", ", wrong. " );
            Log.d("Simon","[DEBUG] state is now " + getStateAsString() );

            setChanged();
            // they got it right
        } else {
            Log.d("Simon", ", correct." );

            // if last displayable, then the win the round
            if (index == length) {
                state = State.WIN;
                // update the score and increase the difficulty
                score++;
                length++;

                Log.d("Simon", "[DEBUG] state is now " + getStateAsString() );
                Log.d("Simon","[DEBUG] new score " + score
                            + ", length increased to " + length
                );
                setChanged();
            }
        }
        notifyObservers();
        return correct;
    }
}
