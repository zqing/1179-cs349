import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

public class MessageView extends JPanel implements Observer {

    // status messages for game
    JLabel fuel = new JLabel("fuel");
    JLabel speed = new JLabel("speed");
    JLabel message = new JLabel("message");
    GameModel model;

    public MessageView(GameModel model) {
        this.model = model;

        // want the background to be black
        setBackground(Color.BLACK);

        setLayout(new FlowLayout(FlowLayout.LEFT));

        add(fuel);
        add(speed);
        add(message);

        for (Component c: this.getComponents()) {
            c.setForeground(Color.WHITE);
            c.setPreferredSize(new Dimension(100, 20));
        }

        model.ship.addObserver(this);
        update(null,null);
        message.setText("(Paused)");
    }


    @Override
    public void update(Observable o, Object arg) {
        fuel.setText("fuel " + model.ship.getFuel());
        speed.setText(String.format("speed %.02f", model.ship.getSpeed()));

        if(model.ship.getFuel() < 10) fuel.setForeground(Color.RED);
        else fuel.setForeground(Color.WHITE);

        if(model.ship.getSpeed() > model.ship.getSafeLandingSpeed()) speed.setForeground(Color.WHITE);
        else speed.setForeground(Color.GREEN);

        switch (model.getStatus()){
            case 0:
                message.setText("(Paused)"); break;
            case 1:
                message.setText(""); break;
            case 2:
                message.setText("Landed!"); break;
            case 3:
                message.setText("Crashed!"); break;
        }
    }
}