import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Observable;
import java.util.Observer;

// the editable view of the terrain and landing pad
public class EditView extends JPanel implements Observer {

    private GameModel model;

    //private Ellipse2D.Double peakHandle[];
    private int selectedIndex;
    private MouseAdapter controller = new MouseAdapter() {
        int lastX, lastY;
        boolean dragged;

        @Override
        public void mouseClicked(MouseEvent e){
            if(e.getClickCount() == 2 ){
                Rectangle2D lp = model.getLandingPad();
                double dx = e.getX() - lp.getCenterX();
                double dy = e.getY() - lp.getCenterY();
                model.moveLandingPad(dx,dy);
                model.snapshot();
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            lastX = e.getX();
            lastY = e.getY();
            dragged = false;
            if(model.getLandingPad().contains(lastX,lastY)){
                selectedIndex = model.getPeakCount();
                repaint();
                return;
            }

            double peakInterval = model.getPeakInterval();
            int peakCount = model.getPeakCount();
            int peakIndex = (int) ((lastX + peakInterval/2) / peakInterval);
            if( peakIndex >= 0 && peakIndex < peakCount && generateHandle(peakIndex).contains(lastX,lastY)){
                selectedIndex = peakIndex;
                repaint();
            }

        }
        @Override
        public void mouseReleased(MouseEvent e) {
            selectedIndex = -1;
            if(dragged) model.snapshot();
            else repaint();
        }
        @Override
        public void mouseDragged(MouseEvent e) {
            boolean result = false;
            if(selectedIndex == model.getPeakCount() ){
                result = model.moveLandingPad(e.getX()-lastX,e.getY()-lastY);
            }
            else if(selectedIndex >= 0 && selectedIndex < model.getPeakCount() ){
                result = model.movePeak(selectedIndex,e.getY()-lastY);
            }
            if(result) {
                lastX = e.getX();
                lastY = e.getY();
                dragged = true;
                repaint();
            }
        }

    };

    public EditView(GameModel model) {

        // want the background to be black
        setBackground(Color.BLACK);

        this.model = model;
        this.selectedIndex = -1;

        addMouseListener(controller);
        addMouseMotionListener(controller);
        model.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        repaint();
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        if(g2 == null) return;
        g2.setColor(Color.LIGHT_GRAY);
        g2.fill(model.getWorldBounds());
        g2.setColor(Color.DARK_GRAY);
        g2.fill(model.getTerrain());

        g2.setColor(Color.GRAY);
        for(int i =0; i<model.getPeakCount();++i) {
            Shape s = generateHandle(i);
            g2.draw(s);
            if (i == selectedIndex) drawThickBound(g2,s);
        }
        g2.setColor(Color.RED);
        g2.fill(model.getLandingPad());
        if (selectedIndex == model.getPeakCount()) drawThickBound(g2,model.getLandingPad());
    }

    private void drawThickBound(Graphics2D g2, Shape shape){
        Stroke s = g2.getStroke();
        Color c = g2.getColor();
        g2.setStroke(new BasicStroke(4));
        g2.setColor(Color.WHITE);
        g2.draw(shape);
        g2.setStroke(s);
        g2.setColor(c);
    }

    private Ellipse2D generateHandle(int i){
        Point2D center = model.getPeak(i);
        return new Ellipse2D.Double(center.getX()-15,center.getY()-15,30,30);
    }


}
