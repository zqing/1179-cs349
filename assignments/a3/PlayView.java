import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.util.Observable;
import java.util.Observer;

// the actual game view
public class PlayView extends JPanel implements Observer {

    private GameModel model;
    private KeyAdapter controller = new KeyAdapter() {
        @Override
        public void keyPressed(KeyEvent e) {
            super.keyPressed(e);

            switch (e.getKeyCode()){
                case KeyEvent.VK_W:
                    model.ship.thrustUp();
                    break;
                case KeyEvent.VK_A:
                    model.ship.thrustLeft();
                    break;
                case KeyEvent.VK_S:
                    model.ship.thrustDown();
                    break;
                case KeyEvent.VK_D:
                    model.ship.thrustRight();
                    break;
                case KeyEvent.VK_SPACE:
                    model.start();
                    model.pause();
                    break;
            }
        }
    };
    public PlayView(GameModel model) {
        this.model = model;
        // needs to be focusable for keylistener
        setFocusable(true);

        // want the background to be black
        setBackground(Color.BLACK);


        model.ship.addObserver(this);
        model.addObserver(this);
        addKeyListener(controller);

    }

    @Override
    public void update(Observable o, Object arg) {
        repaint();

    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        if(g2 == null) return;
        Dimension dim = getSize();
        Point2D pos = model.ship.getPosition();
        g2.translate(dim.width/2,dim.height/2);
        g2.scale(3,3);
        g2.translate(-pos.getX(),-pos.getY());

        g2.setColor(Color.LIGHT_GRAY);
        g2.fill(model.getWorldBounds());
        g2.setColor(Color.DARK_GRAY);
        g2.fill(model.getTerrain());
        g2.setColor(Color.RED);
        g2.fill(model.getLandingPad());

        g2.setColor(Color.BLUE);
        g2.fill(model.ship.getShape());

    }
}
