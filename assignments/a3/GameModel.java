import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import javax.swing.undo.*;

//class Terrain extends Path2D.Double {
//    public final int size;
//    public final Rectangle2D.Double bounds;
//    public int npeaks;
//    public Point2D.Double peaks[];
//
//
//    Terrain(int size,Rectangle2D.Double bounds){
//        this.size = size;
//        this.bounds = bounds;
//        this.npeaks = 0;
//        this.peaks = new Point2D.Double[size];
//    }
//
//    public void addPoint(double x,double y){
//        peaks[npeaks] = new Point2D.Double(x,y);
//        npeaks ++;
//    }
//
//    public void invalidate(){
//        super.reset();
//    }
//
//}

public class GameModel extends Observable {


    public GameModel(int fps, int width, int height, int npeaks) {

        if(npeaks < 2) npeaks=2;

        Random rand = new Random();

        this.undoManager = new UndoManager();

        this.peakCount = npeaks;
        this.peakInterval =  width/(peakCount-1);

        this.terrain = new Polygon();
        this.landingPad = new Rectangle2D.Double(330,100,40,10);

        this.worldBounds = new Rectangle2D.Double(0, 0, width, height);
        this.ship = new Ship(fps, width/2, 50);
        // generate random init status

        for(int i = 0; i < peakCount; ++i ) {
            this.terrain.addPoint((int)(i * peakInterval), (int)(height * (1+rand.nextDouble()) / 2));
        }
        this.terrain.addPoint(width, height);
        this.terrain.addPoint(0, height);


        // anonymous class to monitor ship updates
        ship.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                if(status == 1) {
                    if (!worldBounds.contains(ship.getPosition()) || terrain.intersects(ship.getShape())) {
                        status = 3;
                        ship.stop();
                    }
                    if (landingPad.intersects(ship.getShape())) {
                        if (ship.getSpeed() > ship.getSafeLandingSpeed() || ship.getVelocity().y <= 0) {
                            status = 3;
                        } else {
                            status = 2;
                        }
                        ship.stop();
                    }
                }
                setChangedAndNotify(-1);
            }
        });

        undoCurrent = new CompoundEdit();
        undoCurrent.addEdit(new AbstractUndoableEdit(){
            @Override
            public void undo(){super.undo(); setChangedAndNotify(-1);}
        });

    }

    // Undo/Redo
    private UndoManager undoManager;
    private CompoundEdit undoCurrent;
    public boolean canUndo(){ return undoManager.canUndo(); }
    public boolean canRedo(){ return undoManager.canRedo(); }
    public void undo(){ if(undoManager.canUndo()) undoManager.undo(); }
    public void redo(){ if(undoManager.canRedo()) undoManager.redo(); }
    public void snapshot(){
        undoCurrent.addEdit(new AbstractUndoableEdit(){
            @Override
            public void redo(){ super.redo(); setChangedAndNotify(-1);}
        });
        undoCurrent.end();
        undoManager.addEdit(undoCurrent);
        undoCurrent = new CompoundEdit();
        undoCurrent.addEdit(new AbstractUndoableEdit(){
            @Override
            public void undo(){super.undo(); setChangedAndNotify(-1);}
        });
        setChangedAndNotify(-1);
    }

    // World
    // - - - - - - - - - - -
    //
    private int peakCount;
    private double peakInterval;
    private Polygon terrain;
    private Rectangle2D.Double landingPad;
    private Rectangle2D.Double worldBounds;


    public final Rectangle2D getWorldBounds() { return worldBounds; }

    public final Rectangle2D getLandingPad() { return landingPad ; }

    public final int getPeakCount(){ return peakCount; }

    public final double getPeakInterval(){ return peakInterval; }

    public final Point2D getPeak(int index){
        return new Point2D.Double(terrain.xpoints[index],terrain.ypoints[index]);
    }

    public final Polygon getTerrain(){ return terrain; }

    public boolean moveLandingPad(final double dx, final double dy)
    {
        double x = landingPad.x + dx;
        double y = landingPad.y + dy;
        if (
                ! worldBounds.contains(x,y) ||
                ! worldBounds.contains(x+landingPad.width,y+landingPad.height)
                )  return false;

        undoCurrent.addEdit(new AbstractUndoableEdit() {
            @Override
            public void undo() throws CannotUndoException {
                super.undo();
                landingPad.x -= dx;
                landingPad.y -= dy;
            }

            @Override
            public void redo() throws CannotRedoException {
                super.redo();
                landingPad.x += dx;
                landingPad.y += dy;
            }
        });

        landingPad.x = x;
        landingPad.y = y;
        setChangedAndNotify(peakCount);

        return true;
    }

    public boolean movePeak(final int index, final int delta)
    {
        int y = terrain.ypoints[index] + delta;
        if(y < worldBounds.y || y > worldBounds.y+worldBounds.height) return false;

        undoCurrent.addEdit(new AbstractUndoableEdit() {
            //int saved = terrain.ypoints[index];
            @Override
            public void undo() throws CannotUndoException {
                super.undo();
                terrain.ypoints[index] -= delta;
            }
            @Override
            public void redo() throws CannotUndoException {
                super.redo();
                terrain.ypoints[index] +=  delta;
            }
        });
        terrain.ypoints[index] = y;
        setChangedAndNotify(index);


        return true;

    }

    // Ship
    // - - - - - - - - - - -

    private int status = 0;
    public Ship ship;

    void pause(){
        ship.setPaused(!ship.isPaused());
        if(ship.isPaused()) status = 0;
        else status = 1;
    }

    void start(){
        if(status == 1 || status == 0) return;
        ship.reset(ship.startPosition);
    }
    int getStatus(){ return status;}

    public void shipThrustUp() { ship.thrustUp(); }
    public void shipThrustDown() { ship.thrustDown(); }
    public void shipThrustLeft() { ship.thrustLeft(); }
    public void shipThrustRight() { ship.thrustUp(); }

    // Observerable
    // - - - - - - - - - - -

    // helper function to do both
    private void setChangedAndNotify(Object args) {
        setChanged();
        notifyObservers(args);
    }

}



