#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

void error( const char *msg){
    fprintf(stderr, "[FATAL] %s (%d) ", msg, errno);
    exit(1);
}
#define ERROR(x) error(x)
#define DEBUG(fmt, args...) \
    fprintf(stderr, "[debug]" fmt "\n", args )
///////////////////////////////////////////////////////////////////////////////////////////////
#define GC_NUM 3
#define GC_BLACK 0
#define GC_WHITE 1
#define GC_THICK 2

#define FONT_NUM 1

#include <X11/Xlib.h>
#include <X11/keysym.h>

struct XInfo
{
    Display *display;
    int screen;
    Window window;
    GC gc[GC_NUM];
    Font font[FONT_NUM];

};



void x_init( XInfo *xinfo, unsigned short width, unsigned short height )
{
    xinfo->display = XOpenDisplay( "" );
    if ( !xinfo->display ){
        ERROR( "Cannot open display." );
        return;
    }

    xinfo->screen = XDefaultScreen( xinfo->display );
    unsigned long white = XWhitePixel( xinfo->display, xinfo->screen );
    unsigned long black = XBlackPixel( xinfo->display, xinfo->screen);

    xinfo->window = XCreateSimpleWindow(
            xinfo->display,
            DefaultRootWindow(xinfo->display),
            10, 10,
            width, height,
            2,
            black, white
    );

    for(size_t i=0; i < GC_NUM; ++i ) {
        xinfo->gc[i] = XCreateGC(xinfo->display, xinfo->window, 0, 0);
        XSetBackground( xinfo->display, xinfo->gc[i], white );
    }

    XSetForeground( xinfo->display, xinfo->gc[GC_BLACK], black );
    XSetForeground( xinfo->display, xinfo->gc[GC_WHITE], white );
    XSetForeground( xinfo->display, xinfo->gc[GC_THICK], black );
    XSetLineAttributes( xinfo->display, xinfo->gc[GC_THICK], 4, LineSolid, CapRound, JoinRound);

    xinfo->font[0] = XLoadFont(xinfo->display, "*x24");
    XSetFont(xinfo->display, xinfo->gc[GC_BLACK],xinfo->font[0]);


    XSelectInput(
            xinfo->display, xinfo->window,
            ButtonPressMask | ButtonReleaseMask | ButtonMotionMask |
            KeyPressMask | KeyReleaseMask |  PointerMotionMask | StructureNotifyMask |
            ExposureMask
    );

    XMapRaised( xinfo->display, xinfo->window );
    XFlush( xinfo->display );
}


///////////////////////////////////////////////////////////////////////////////////////////////
#include "simon.h"

class MySimon : public Simon
{
    int ticks;
public:
    MySimon(int _buttons): Simon(_buttons), ticks{60} { }

    void next(XInfo &xinfo)
    {
        if( state == COMPUTER) {
            if (ticks > 0) --ticks;
            else {
                nextButton();
                ticks = 60;
            }
        }
    }

    int curretButton() const { return sequence[index]; }

    int remainTicks() const { return ticks; }

    void notifyXKeyRelease( XKeyEvent &event )
    {
        switch(state) {
            case WIN: case LOSE: case START:
                if (XK_space == XLookupKeysym(&event, 1)) newRound();
            default:
                break;
        }
    }

};

///////////////////////////////////////////////////////////////////////////////////////////////
#include <string>
#include <cmath>

class Displayable
{
protected:
    bool dirty = true;
public:
    // Draw current frame
    virtual void draw( XInfo & ) {}
    // Generate next frame
    virtual void next( XInfo &, MySimon &) {  }
    // Position
    // virtual int isOverlap( XRectangle & ) { return 0; }
    virtual int isOverlap( int x, int y ) { return 0; }
    // XEvent handlers
    virtual void notifyXButtonPress( XButtonEvent &, MySimon & ) {}
    virtual void notifyXButtonRelease( XButtonEvent &, MySimon & ) {}
    virtual void notifyXMotion( XMotionEvent &, MySimon & ) {}
    virtual void notifyXConfigure( XConfigureEvent &, MySimon & ) {}
    virtual void notifyXExpose( XConfigureEvent &, MySimon & ) {  dirty = true; }
};

class Text : public Displayable
{
protected:
    XPoint pos;
    std::string buffer;
public:
    Text(XPoint pos, std::string str ):
            pos{pos}, buffer{ std::move(str) }
    {}

    void draw( XInfo &xinfo ) override
    {
        if(!dirty) return;
        XFillRectangle(xinfo.display,xinfo.window,xinfo.gc[GC_WHITE],pos.x,pos.y-22,430,25);
        XDrawImageString(
                xinfo.display, xinfo.window, xinfo.gc[GC_BLACK],
                pos.x, pos.y,
                buffer.c_str(), (int)buffer.length()
        );
        dirty = false;
    }

};

class ScoreText : public Text
{
    int last;
public:

    explicit ScoreText(XPoint pos): Text{pos, {}}, last{-1} {}

    void next(XInfo &, MySimon &simon) override
    {
        int score = simon.getScore();
        if(score == last) return;
        last = score;
        buffer = "Score: " + std::to_string(last);
        dirty = true;
    }

    void notifyXConfigure(XConfigureEvent &event, MySimon &simon ) override
    {

        XClearArea(event.display,event.window,pos.x,pos.y-22,430,25,false);
        pos.x = event.width / (simon.getNumButtons() + 4);
        pos.y = event.height >> 3;
        dirty = true;
    }
};

class PromptText : public Text
{
    Simon::State last;
public:

    explicit PromptText(XPoint pos): Text{pos, "Press SPACE to play"}, last{Simon::START} {}

    void next(XInfo &, MySimon &simon) override
    {
        Simon::State now = simon.getState();
        if(last == now ) return;
        switch (now)
        {
            case Simon::WIN:
                buffer = "You won! Press SPACE to continue." ;
                break;
            case Simon::LOSE:
                buffer = "You lose. Press SPACE to play again.";
                break;
            case Simon::COMPUTER:
                buffer = "Watch what I do ...";
                break;
            case Simon::HUMAN:
                buffer = "Your turn ...";
        }
        dirty = true;
    }

    void notifyXConfigure(XConfigureEvent &event, MySimon &simon ) override
    {
        XClearArea(event.display,event.window,pos.x,pos.y-22,430,25,false);
        pos.x = event.width / (simon.getNumButtons() + 4) + 200;
        pos.y = event.height >> 3;
        dirty = true;
    }

};

class Ball : public Displayable
{
    short seq;
    XPoint pos;
    unsigned short radius;
    struct { int ticks=0; } anim1;
    struct { bool active = false; double ticks = 0; int scale = 20;} anim2;
    XArc arc;
    int gc_draw;
    std::string label;

    void resetDrawParam()
    {
        arc.x = pos.x - radius;
        arc.y = pos.y - radius;
        arc.width = radius << 1;
        arc.height = radius << 1;
        arc.angle1 = 0;
        arc.angle2 = 360 << 6;
        label = std::to_string(seq+1);
    }

public:
    Ball( XPoint _pos, unsigned short _radius, short _seq):
            seq{_seq}, pos{_pos}, radius{_radius}, gc_draw{GC_BLACK}, anim1{}, anim2{}
    {
        resetDrawParam();
    }

    void draw(XInfo &xinfo) override
    {
        if ( !dirty ) return;
        XFillRectangle(xinfo.display,xinfo.window,xinfo.gc[GC_WHITE],arc.x-4,arc.y-4,arc.width+8,arc.height+8);
        //XClearArea( xinfo.display,xinfo.window,arc.x-4,arc.y-4,arc.width+8,arc.height+8,0);
        XDrawArcs( xinfo.display, xinfo.window, xinfo.gc[gc_draw], &arc, 1);
        if(!label.empty())
            XDrawImageString(
                    xinfo.display, xinfo.window, xinfo.gc[GC_BLACK],
                    arc.x + ( arc.width >> 1 ), arc.y + ( arc.height >> 1 ),
                    label.c_str(), (int)label.length()
            );
        if(anim1.ticks > 0) {
            unsigned int offset = arc.width * (30 - anim1.ticks) / 30 ;
            unsigned int offset2 = offset >> 1;
            XFillArcs( xinfo.display, xinfo.window, xinfo.gc[GC_BLACK], &arc, 1);
            XDrawArc(xinfo.display, xinfo.window, xinfo.gc[GC_WHITE],
                     arc.x + offset2, arc.y + offset2,
                     arc.width - offset , arc.height - offset,
                     arc.angle1, arc.angle2
            );
        }
        dirty = false;
    }

    void next(XInfo & xinfo, MySimon &simon) override
    {

        if(simon.getState() != Simon::COMPUTER && simon.getState() != Simon::HUMAN)
        {
            if(!anim2.active) anim2.active = true;
        } else {
            if(anim2.active) {
                anim2.active = false;
                dirty = true;
                XFillRectangle(xinfo.display,xinfo.window,xinfo.gc[GC_WHITE],arc.x-4,arc.y-4,arc.width+8,arc.height+8);
                resetDrawParam();
            }
            if( simon.curretButton() == seq && simon.remainTicks() == 30)
            {
                anim1.ticks = 30;
            }
        }

        // Animations
        if( anim1.ticks > 0 )
        {
            -- anim1.ticks;
            dirty = true;
        }
        if( anim2.active )
        {
            XFillRectangle(xinfo.display,xinfo.window,xinfo.gc[GC_WHITE],arc.x-4,arc.y-4,arc.width+8,arc.height+8);
            arc.y = (pos.y + (short)(anim2.scale * sin(anim2.ticks+seq))) - radius;
            anim2.ticks += 0.05;
            dirty = true;
        }

    }

    int isOverlap( int _x, int _y ) override
    {
        unsigned short radius = arc.width >> 1;
        short x = arc.x + radius;
        short y = arc.y + radius;
        double dist = sqrt(pow( _x - x, 2) + pow( _y - y, 2));
        return dist < radius;
    }

    void notifyXButtonPress( XButtonEvent &event, MySimon & simon ) override
    {
        if ( !isOverlap(event.x, event.y) ) return;

        anim1.ticks = 30;
        if ( simon.getState() == Simon::HUMAN )  simon.verifyButton(seq);
    }
//    void notifyXButtonRelease( XButtonEvent &event, MySimon & ) override
//    {
//        if( gc_fill < 0 ) return;
//        gc_fill = -1;
//        dirty = true;
//    }
    void notifyXMotion( XMotionEvent &event, MySimon & ) override {
        if ( isOverlap(event.x, event.y) ){
            if( gc_draw == GC_THICK) return;
            gc_draw = GC_THICK;
            dirty = true;
        }else{
            if( gc_draw == GC_BLACK) return;
            gc_draw = GC_BLACK;
            dirty = true;
        }
    }

    void notifyXConfigure( XConfigureEvent &event, MySimon &simon) override
    {
        int num = simon.getNumButtons();
        int vspace = event.width / num;
        XClearArea(event.display,event.window,arc.x-2,arc.y-2,arc.width+4,arc.height+4,false);
        //XFillRectangle(event.display,event.window,event.,arc.x-4,arc.y-4,arc.width+8,arc.height+8);
        pos.x = (short) (vspace * seq  + vspace/2);
        pos.y = (short)( event.height >> 1 );
        anim2.scale = event.height >> 3;
        resetDrawParam();
        dirty = true;
    }

};

///////////////////////////////////////////////////////////////////////////////////////////////
#include <unistd.h>
#include <signal.h>
#include <time.h>

#include <vector>
#define FPS 60

namespace Game
{
    //// Set POSIX Timer
    static timer_t timer = nullptr;
    static bool dirty = true;

    void alarm_handler(int){
        if(dirty){DEBUG("%s","frame dropped."); dirty=false; }
        else dirty = true;
    }

    void set_timer(long val){
        sigevent_t sigev = {};
        sigev.sigev_notify = SIGEV_SIGNAL;
        sigev.sigev_signo = SIGALRM;

        timespec time = {0, val};
        itimerspec itime = {time, time};

        if(timer_create(CLOCK_REALTIME, &sigev , &timer)) ERROR("timer_create");
        if(timer_settime(timer, 0, &itime, 0)) ERROR("timer_setime");
        signal( SIGALRM, alarm_handler );
    }
    //// The main game loop
    void run(XInfo &xinfo, unsigned short balls) {
        MySimon simon = {balls};
        std::vector<Displayable *> dlist = {};


        dlist.push_back(new ScoreText({}));
        dlist.push_back(new PromptText({}));

        for (unsigned short i = 0; i < balls; ++i)
        {
            auto *ball = new Ball({}, 50, i);
            dlist.push_back(ball);
        }

        set_timer(1000000000 / FPS);

        while (true)
        {
            //// XEvent dispatch
            if ( XPending(xinfo.display) ) {
                XEvent event;
                XNextEvent( xinfo.display, &event );
                switch ( event.type ) {
                    case Expose:
                        for(auto d : dlist) d->notifyXExpose(event.xconfigure, simon);
                        break;
                    case ConfigureNotify:
                        for(auto d : dlist) d->notifyXConfigure(event.xconfigure, simon);
                        //DEBUG("Reconfig size: %dx%d", event.xconfigure.height,event.xconfigure.width);
                        break;
                    case ButtonPress:
                        for(auto d : dlist) d->notifyXButtonPress(event.xbutton, simon);
                        //DEBUG("ButtonPress state %d",event.xbutton.state );
                        //DEBUG("ButtonPress button %d",event.xbutton.button );
                        break;
                    case ButtonRelease:
                        for(auto d : dlist) d->notifyXButtonRelease(event.xbutton, simon);
                        break;
                    case MotionNotify:
                        for(auto d : dlist) d->notifyXMotion(event.xmotion, simon);
                        break;
                    case KeyRelease:
                        KeySym k = XLookupKeysym(&event.xkey, 1);
                        simon.notifyXKeyRelease(event.xkey);
                        //DEBUG("KeyRelease code 0x%x sym 0x%04x at %d", event.xkey.keycode, (unsigned int)k,
                        //            (int) event.xkey.time);
                        if(k == XK_Q) return;
                }
                //dirty = true;
            }
            if(dirty){
                //// Game Logic
                simon.next(xinfo);
                for (auto &i: dlist) i->next(xinfo, simon);
                //// XDraw Screen
                //XClearWindow(xinfo.display, xinfo.window);
                for (auto &i: dlist) i->draw(xinfo);
                //XCopyArea( xinfo.display, xinfo.buffer, xinfo.window, xinfo.gc[GC_WHITE], 0, 0,xinfo.size.w, xinfo.size.h, 0, 0);
                XFlush(xinfo.display);
                dirty = false;
            }else pause();

        }

    }

}

///////////////////////////////////////////////////////////////////////////////////////////////

int main( int argc, const char *argv[] )
{
    unsigned long numball = 0;
    XInfo xinfo = {};

    if( argc != 2 ) ERROR("Wrong number of arguments");
    numball = std::stoul(argv[1]);
    if( numball > 0xffffffff ) ERROR("Invalid arguments");

    srand(time(nullptr));
    x_init(&xinfo, 800, 400);

    Game::run(xinfo, (unsigned short)numball);
    fprintf(stderr, "Terminated normally.\n");
    XCloseDisplay(xinfo.display);

}
